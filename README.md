**Jiggle User Guide**

Temporary home for Jiggle User Guide

(interactive analyst user interface for AQMS)

Upon commit, the ``.gitlab-ci.yml`` file also builds a copy of the documentation at https://aqms.swg.gitlab.io/jiggle-userdocs/
[You may get a warning about this documentation website not being secure. It is ok to proceed anyway and visit the site.]

**Build/Deploy instructions for Jiggle user guide**

Official location for documentation: https://pasadena.wr.usgs.gov/jiggle/UserGuide/site/
(Replace ``cyoon`` with your username on agent86)
```
$ mkdocs build --clean
$ scp -r ./site/ cyoon@agent86.gps.caltech.edu:/webfiles/pasadena/jiggle/UserGuide
```
