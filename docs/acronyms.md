# Acronyms
* ANSS - Advanced National Seismic System
* API - Application Programming Interface
* AQMS - ANSS Quake Monitoring System
* CISN - California Integrated Seismic Network -
  [https://www.cisn.org/](https://www.cisn.org/)
* DRP - Duty Review Page
* GUI - Graphical User Interface
* JASI - Java Abstract Seismic Interface
* JDBC - Java Database Connectivity
* JVM - Java Virtual Machine
* PDL - Product Distribution Layer -
  [https://usgs.github.io/pdl/](https://usgs.github.io/pdl/)
* PNSN - Pacific Northwest Seismic Network -
  [https://pnsn.org/](https://pnsn.org/)
* PP - Post Processing
* RSN - Regional Seismic Network
* RT - Real Time
* SCEDC - Southern California Earthquake Data Center -
  [http://scedc.caltech.edu/](http://scedc.caltech.edu/)
* SCSN - Southern California Seismic Network -
  [http://www.scsn.org/](http://www.scsn.org/)
* TRP - Trigger Review Page
* UTC - Coordinated Universal Time
* VPN - Virtual Private Network
* WGS84 - World Geodetic System 1984
